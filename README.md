# ProjetDevJava
Projet de fin de formation:

Application de type CRM pour la gestion de clients , devis, facturations et articles.

En cours de développement:
  - Application web avec Serveur TOMCAT9, MYSQL, SPRING, JSP BOOTSTRAP et REST API 
  - Application Angular avec BOOTSTRAP pour effectuer une connexion avec le REST API

Documentation génerale  développée avec Sphinx pour expliquer le fonctionnement et les points compliqués de l'application
Documentation Javadoc pour la partie développement Java.

